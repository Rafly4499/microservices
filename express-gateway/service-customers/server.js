const express = require('express')
const app = express()
const port = 3001

const customers = [

  {
    "id": 1,
    "name": "Bambang",
    "username": "bambang",
    "email": "bambang@gmail.com"
  }, {
    "id": 2,
    "name": "Budi",
    "username": "budi",
    "email": "budi@gmail.com"
  }, {
    "id": 3,
    "name": "Rosa",
    "username": "rosa",
    "email": "rosa@gmail.com"
  }, {
    "id": 4,
    "name": "Andine",
    "username": "andine",
    "email": "andine@gmail.com"
  }

]
app.get('/api/customers', (req, res) => res.send({
  results: customers
}))

app.listen(port, () => console.log('Example app listening on port ${port}!'))