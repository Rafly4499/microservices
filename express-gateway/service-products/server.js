const express = require('express')
const app = express()
const port = 3002

const products = [

  {
    "id": 1,
    "name_products": "Iphone",
    "description": "Hp Iphone kualitas terbaik"
  }, {
    "id": 2,
    "name_products": "Asus",
    "description": "Hp Asus kualitas terbaik"
  }, {
    "id": 3,
    "name_products": "Xiaomi",
    "description": "Hp Xiaomi kualitas terbaik"
  }, {
    "id": 4,
    "name_products": "Samsung",
    "description": "Hp Samsung kualitas terbaik"
  }
]
app.get('/api/products', (req, res) => res.send({
  results: products
}))

app.listen(port, () => console.log('Example app listening on port ${port}!'))