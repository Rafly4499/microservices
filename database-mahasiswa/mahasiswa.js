const express = require('express');
const app = express()
const port = 5000
const dataMahasiswa = require('./mahasiswa');
app.get('/mahasiswa', (req, res) => {
    console.log('request to /mahasiswa');
    const q = req.query.q;
    if (q) {
        const filtered = dataMahasiswa
            .filter(r => {
                const loweCaseName = r.nama.toLowerCase();
                return loweCaseName.search(q.toLowerCase()) !== -1
            });

        return res.json(filtered);
    }
    return res.json(dataMahasiswa);
});

app.get('/mahasiswa/:id', (req, res) => {
    const id = parseInt(req.params.id);
    console.log('request to /mahasiswa/', id);
    const filtered = dataMahasiswa.filter(r => r.id === id);
    res.json(filtered);
});

app.listen(port, () => console.log(`Example app listening at http:://localhost:${port}`));