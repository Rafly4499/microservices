var express = require('express');
var router = express.Router()
var kelasService = require('./kelas-service')
var mahasiswaService = require('./mahasiswa-service')

router.use(kelasService)
router.use(mahasiswaService)

module.exports = router