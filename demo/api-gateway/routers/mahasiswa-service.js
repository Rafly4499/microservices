var express = require('express');
var router = express.Router()
var qs = require('qs');
var apiAdapter = require('./api-adapter');

const BASE_URL = 'http://localhost:5000'
const api = apiAdapter(BASE_URL);

router.get('/mahasiswa', async (req, res) => {
    try {
        const mahasiswa = await api.get(req.path);
        res.send(mahasiswa.data);
    } catch (error) {
        res.send('service mahasiswa not available');
    }
})

router.get('/mahasiswa', async (req, res) => {
    try {
        console.log(qs.stringify(req.body));
        const mahasiswa = await api.post(req.path, qs.stringify(req.body), {
            headers: {
                'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
            }
        });
        res.send(mahasiswa.data);
    } catch (error) {
        res.send('service mahasiswa not available');
    }
})

module.exports = router